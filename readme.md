<h2>栄養素入力アプリ</h2>
<h3>Tracking Variables </h3>
・Wake-up & Sleep time
・Body weight

・Meals (what & when)
・Exercise (what & when)
・Water intake (when)

<h3>Visualization</h3>

・Day-by-day body weight
・Nutritional intake calc (table & graph)


<h2>Code Variables </h2>

<h3>File Prefixes</h3>
・c_ : css files
・h_ : html files
・j_ : js files

<h3>Special Files </h3>
・c_general.css: Generalized CSS file that fits all pages. Must be loaded for all HTML sites.
・h_general.html: General template for all html files. Not connected to any other pages
・j_general.js: Generalized JS file that fits all pages. Must be loaded for all HTML sites.

<h3>Pages </h3>
・index.html: Loader site. <b>Shows the first page that user views once logged in. </b>
・input.html: Site that user inputs all information every day. <i>Currently only supports once per every day </i>
・result.html: Long-term result that comes up. 
・day.html: Day-to-day result, that shows today's data & yesterday's data.
